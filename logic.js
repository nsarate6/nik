var faces = document.querySelectorAll('.face');
    var testamonial = document.querySelector('.testamonial');
    var testamonialHeadline = testamonial.querySelector('h3');
    var testamonialName = testamonial.querySelector('a');

    var setFace = function(current) {
      for (var i=0; i<faces.length; i++) {
        faces[i].classList.remove('selected');
      }

      faces[current].classList.add('selected');
    };

    var setTestamonial = function(quote) {
      testamonial.classList.add('invisible');

      setTimeout(function() {
        testamonialHeadline.innerHTML = quote.quote;
        testamonialName.innerHTML = quote.name;
        testamonial.classList.remove('invisible');
      }, 350);
    };

    var testamonialsCurrent  = 1;

    var quotes = [
      {
        name: 'Quote One',
        quote: '"SOME THINGS ARE JUST BETTER WITH US."'
      },
      {
        name: 'QUOTE TWO',
        quote: '"I can already see it improving my life when I\'m communicating with people."'
      },
      {
        name: 'QUOTE THREE',
        quote: '"It\'s also helped my speaking, because I\'m little bit more considerate about overusing words."'
      },
      {
        name: 'QUOTE FOUR',
        quote: '"Elevate allows me to better my communication personally and professionally."'
      },
      {
        name: 'QUOTE FIVE',
        quote: '"I\'m seeing myself improve in different ways that I didn\'t know I could."'
      }
    ];
    jQuery(document).ready(function($) {
        "use strict";
        //  TESTIMONIALS CAROUSEL HOOK
        $('#customers-testimonials').owlCarousel({
            loop: true,
            center: true,
            items: 3,
            margin: 0,
            autoplay: true,
            dots:true,
            autoplayTimeout: 8500,
            smartSpeed: 450,
            responsive: {
              0: {
                items: 1
              },
              768: {
                items: 2
              },
              1170: {
                items: 3
              }
            }
        });
    });
   
